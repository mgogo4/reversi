package world.mgogo.reversi.enums;

public enum UITaskType {

    DRAW_GAME_BOARD(0),
    SHOW_SPINNER(1),
    HIDE_SPINNER(2),
    RESOLVE_WINNER(3);

    private int id;

    UITaskType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}