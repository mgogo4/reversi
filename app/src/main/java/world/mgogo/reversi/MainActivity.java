package world.mgogo.reversi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import world.mgogo.reversi.utils.ReversiConstants;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        Button pvpButton = findViewById(R.id.start_pvp_button);
        pvpButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            intent.putExtra(ReversiConstants.WHITE_AI_KEY, false);
            intent.putExtra(ReversiConstants.BLACK_AI_KEY, false);
            startActivity(intent);
            finish();
        });

        Button aivaiButton = findViewById(R.id.start_aivai_button);
        aivaiButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            intent.putExtra(ReversiConstants.WHITE_AI_KEY, true);
            intent.putExtra(ReversiConstants.BLACK_AI_KEY, true);
            startActivity(intent);
            finish();
        });

        Button asBlackButton = findViewById(R.id.start_asBlack_button);
        asBlackButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            intent.putExtra(ReversiConstants.WHITE_AI_KEY, true);
            intent.putExtra(ReversiConstants.BLACK_AI_KEY, false);
            startActivity(intent);
            finish();
        });

        Button asWhiteButton = findViewById(R.id.start_asWhite_button);
        asWhiteButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            intent.putExtra(ReversiConstants.WHITE_AI_KEY, false);
            intent.putExtra(ReversiConstants.BLACK_AI_KEY, true);
            startActivity(intent);
            finish();
        });
    }
}
