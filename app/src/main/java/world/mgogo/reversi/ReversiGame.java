package world.mgogo.reversi;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import sac.game.AlphaBetaPruning;
import sac.game.GameSearchAlgorithm;
import sac.game.GameSearchConfigurator;
import world.mgogo.reversi.enums.UITaskType;
import world.mgogo.reversi.model.Player;
import world.mgogo.reversi.utils.ReversiConstants;
import world.mgogo.reversi.model.ReversiGameState;

public class ReversiGame extends RelativeLayout implements Runnable {
    private final ReversiGameState reversiGameState = new ReversiGameState();
    private final GameSearchAlgorithm algorithm = new AlphaBetaPruning(reversiGameState);
    private final UIHandler uiHandler = new UIHandler(this);
    private TileButton[][] boardButtons;
    private TextView actualPlayerValue;
    private TextView whiteScore;
    private TextView blackScore;
    private TextView lastMoves;
    private Player whitePlayer;
    private Player blackPlayer;
    private Thread gameThread = null;
    private ProgressBar spinner;
    private volatile boolean playing;
    private volatile boolean playerMoved = false;
    private volatile List<Double> depths = new ArrayList<>();
    private volatile List<Long> movesTime = new ArrayList<>();
    private volatile List<String> lastMovesArray = new ArrayList<>();
    private volatile long gameTime = System.currentTimeMillis();
    private boolean paused = true;
    private final Random randomMovePicker;

    public ReversiGame(Context context, boolean isWhitePlayerAI, boolean isBlackPlayerAI) {
        super(context);
        whitePlayer = new Player(isWhitePlayerAI, ReversiConstants.WHITE);
        blackPlayer = new Player(isBlackPlayerAI, ReversiConstants.BLACK);
        GameSearchConfigurator gameSearchConfigurator = new GameSearchConfigurator();
        gameSearchConfigurator.setDepthLimit(2D);
        gameSearchConfigurator.setTranspositionTableOn(false);
        gameSearchConfigurator.setQuiescenceOn(true);
        gameSearchConfigurator.setTimeLimit(6000L);
        gameSearchConfigurator.setParentsMemorizingChildren(false);
        gameSearchConfigurator.setRefutationTableOn(true);
        gameSearchConfigurator.setRefutationTableDepthLimit(1.5D);
        algorithm.setConfigurator(gameSearchConfigurator);
        randomMovePicker = new Random();

        init();
    }

    private void init() {
        inflate(getContext(), R.layout.reversi_game, this);
        boardButtons = new TileButton[ReversiConstants.SIZE][ReversiConstants.SIZE];
        initBoard();

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        actualPlayerValue = findViewById(R.id.playerValue);
        actualPlayerValue.setText(getPlayer().getName());

        whiteScore = findViewById(R.id.whiteScore);
        whiteScore.setText(String.valueOf(getPlayerScore(ReversiConstants.WHITE)));

        blackScore = findViewById(R.id.blackScore);
        blackScore.setText(String.valueOf(getPlayerScore(ReversiConstants.BLACK)));

        lastMoves = findViewById(R.id.lastMoves);
        lastMoves.setText(getLastMovesString());

    }

    private void initBoard() {
        LinearLayout layoutVertical = findViewById(R.id.liVLayout);
        LinearLayout rowLayout = null;
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, 1);

        for (int i = 0; i < ReversiConstants.SIZE; ++i) {
            for (int j = 0; j < ReversiConstants.SIZE; ++j) {
                boardButtons[i][j] = new TileButton(getContext());
                boardButtons[i][j].setTag(i * ReversiConstants.SIZE + j);
                boardButtons[i][j].setOnClickListener(v -> {
                    if (v.getTag() instanceof Integer) {
                        turnOffButtons();
                        move((Integer) v.getTag());
                        playerMoved = true;
                    }
                });

                refreshTile(i, j);
                if (j == 0) {
                    rowLayout = new LinearLayout(getContext());
                    rowLayout.setWeightSum(ReversiConstants.SIZE);
                    layoutVertical.addView(rowLayout, param);
                }
                rowLayout.addView(boardButtons[i][j], param);
            }
        }
    }

    public Player getPlayer() {
        if (ReversiConstants.WHITE == reversiGameState.getActualPlayer()) {
            return whitePlayer;
        } else {
            return blackPlayer;
        }
    }

    public int getPlayerScore(byte colour) {
        return reversiGameState.getPlayerScore(colour);
    }

    private void turnOffButtons() {
        for (int i = 0; i < ReversiConstants.SIZE; ++i) {
            for (int j = 0; j < ReversiConstants.SIZE; ++j) {
                boardButtons[i][j].setClickable(false);
            }
        }
    }

    public void move(int n) {
        lastMovesArray.add(String.valueOf(n));
        reversiGameState.makeMove(n);
    }

    private void refreshTile(int x, int y) {
        TileButton tileButton = boardButtons[x][y];
        tileButton.setClickable(false);

        int tileButtonNumber = (Integer) tileButton.getTag();

        byte colour = getState(tileButtonNumber);
        if (colour == ReversiConstants.BLACK) {
            tileButton.setBackgroundResource(R.drawable.black);
        } else if (colour == ReversiConstants.WHITE) {
            tileButton.setBackgroundResource(R.drawable.white);
        } else if (colour == ReversiConstants.EMPTY) {
            if (!getPlayer().isAiPlayer() && isPossibleMove(tileButtonNumber)) {
                tileButton.setBackgroundResource(R.drawable.available);
                tileButton.setClickable(true);

            } else {
                tileButton.setBackgroundResource(R.drawable.empty);
            }
        }
    }

    public byte getState(int n) {
        return reversiGameState.getState(n);
    }

    public boolean isPossibleMove(int n) {
        return reversiGameState.getPlayerAvailableMoves().contains(n);
    }

    @Override
    public void run() {
        // playing gives us finer control rather than just relying on the calls to run
        // playing must be true AND the thread for the main loop to execute
        movesTime = new ArrayList<>();
        depths = new ArrayList<>();
        while (playing) {
            // provided the game isn't paused - process the player turn
            if (!paused) {
                playerMoved = false;
                // send message to GUI thread to draw actual game board
                uiHandler.sendMessage(buildMessage(UITaskType.DRAW_GAME_BOARD));

                if (reversiGameState.isFinished()) {
                    gameTime = System.currentTimeMillis() - gameTime;
                    // display congratulations for winner and prepare new game
                    uiHandler.sendMessage(buildMessage(UITaskType.RESOLVE_WINNER));
                    long sum = movesTime.get(0);

                    Long[] sortedTimes = movesTime.toArray(new Long[0]);
                    Arrays.sort(sortedTimes);

                    double median;
                    if (sortedTimes.length % 2 == 0) {
                        median = ((double) sortedTimes[sortedTimes.length / 2] + (double) sortedTimes[sortedTimes
                                .length / 2 - 1]) / 2;

                    } else {
                        median = (double) sortedTimes[sortedTimes.length / 2];
                    }

                    for (int counter = 1; counter < movesTime.size(); ++counter) {
                        sum += movesTime.get(counter);
                    }

                    Log.i("Info", String.format("Number of moves processed: %s.", sortedTimes.length));
                    Log.i("Info", String.format("Longest move time: %s.", sortedTimes[sortedTimes.length - 1]));
                    Log.i("Info", String.format("Shortest move time: %s.", sortedTimes[0]));
                    Log.i("Info", String.format("Average move time: %s.", (double) sum / sortedTimes.length));
                    Log.i("Info", String.format("Median of moves time: %s.", median));
                    pause();
                } else if (!reversiGameState.getPlayerAvailableMoves().isEmpty()) {
                    if (getPlayer().isAiPlayer()) {
                        // send message to GUI thread to display loading spinner
                        uiHandler.sendMessage(buildMessage(UITaskType.SHOW_SPINNER));
                        // process AI move
                        long frameStartTime = System.currentTimeMillis();
                        move();
                        movesTime.add(algorithm.getDurationTime());
                        depths.add(algorithm.getDepthReached());
                        // send message to GUI thread to hide loading spinner
                        uiHandler.sendMessage(buildMessage(UITaskType.HIDE_SPINNER));
                    } else {
                        while (!playerMoved) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                Log.e("Error", "Interrupted sleeping thread.");
                                Thread.currentThread().interrupt();
                            }
                        }
                    }
                }
            }
        }
    }

    public Message buildMessage(UITaskType uiTaskType) {
        Message msg = uiHandler.obtainMessage();
        msg.obj = uiTaskType;
        return msg;
    }

    public void pause() {
        playing = false;
        paused = true;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error", "joining thread");
            Thread.currentThread().interrupt();
        }

    }

    public void move() {
        algorithm.execute();
        String move = algorithm.getBestMoves().get(randomMovePicker.nextInt(algorithm.getBestMoves().size()));
        lastMovesArray.add(move);
        reversiGameState.makeMove(Integer.parseInt(move));
    }

    public byte getWinner() {
        return reversiGameState.getWinner();
    }

    public void restart() {
        reversiGameState.restartGame();
        reset();
        resume();
    }

    public void handleUIMessage(Message msg) {
        switch (((UITaskType) msg.obj).getId()) {
            case 0:
                draw();
                break;
            case 1:
                spinner.setVisibility(View.VISIBLE);
                break;
            case 2:
                spinner.setVisibility(View.GONE);
                break;
            case 3:
                winnerDialog();
                break;
            default:
                break;
        }
        draw();
    }

    // Draw the game objects and the HUD
    void draw() {
        actualPlayerValue.setText(getPlayer().getName());
        whiteScore.setText(String.valueOf(getPlayerScore(ReversiConstants.WHITE)));
        blackScore.setText(String.valueOf(getPlayerScore(ReversiConstants.BLACK)));
        lastMoves.setText(getLastMovesString());

        for (int i = 0; i < ReversiConstants.SIZE; ++i) {
            for (int j = 0; j < ReversiConstants.SIZE; ++j) {
                refreshTile(i, j);
            }
        }
    }

    private void reset() {
        for (int i = 0; i < ReversiConstants.SIZE; ++i) {
            for (int j = 0; j < ReversiConstants.SIZE; ++j) {
                refreshTile(i, j);
            }
        }

        spinner.setVisibility(View.GONE);
        actualPlayerValue.setText(getPlayer().getName());
        whiteScore.setText(String.valueOf(getPlayerScore(ReversiConstants.WHITE)));
        blackScore.setText(String.valueOf(getPlayerScore(ReversiConstants.BLACK)));
        lastMoves.setText(getLastMovesString());
    }

    public void resume() {
        playing = true;
        paused = false;

        gameThread = new Thread(this);
        gameThread.start();
    }

    public void winnerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        byte winner = getWinner();
        String messageString;
        if (winner == ReversiConstants.EMPTY) {
            messageString = "Game ended with a draw.";
        } else {
            messageString = Player.playerName(winner) + " player won.";
        }

        Long[] sortedTimes = movesTime.toArray(new Long[0]);
        Double[] sortedDepths = depths.toArray(new Double[0]);
        Arrays.sort(sortedTimes);
        Arrays.sort(sortedDepths);
        double timeMedian;
        if (sortedTimes.length % 2 == 0) {
            timeMedian = ((double) sortedTimes[sortedTimes.length / 2] + (double) sortedTimes[sortedTimes
                    .length / 2 - 1]) / 2;

        } else {
            timeMedian = (double) sortedTimes[sortedTimes.length / 2];
        }

        double depthMedian;
        if (sortedDepths.length % 2 == 0) {
            depthMedian = ((double) sortedDepths[sortedDepths.length / 2] + (double) sortedDepths[sortedDepths
                    .length / 2 - 1]) / 2;

        } else {
            depthMedian = (double) sortedDepths[sortedDepths.length / 2];
        }

        long timeSum = movesTime.get(0);
        double depthSum = depths.get(0);
        for (int counter = 1; counter < movesTime.size(); ++counter) {
            timeSum += movesTime.get(counter);
            depthSum += depths.get(counter);
        }

        messageString = String.format("%s%n" +
                        "Average depth: %s.%n" +
                        "Median depth: %s.%n" +
                        "Max depth: %s.%n" +
                        "Average time: %s.%n" +
                        "Median time: %s.%n" +
                        "Max time: %s.%n" +
                        "Moves Number: %s.%n" +
                        "Game time: %s.",
                messageString,
                (double) depthSum / sortedDepths.length, depthMedian, sortedDepths[sortedDepths.length - 1],
                (double) timeSum / sortedTimes.length, timeMedian, sortedTimes[sortedTimes.length - 1],
                sortedTimes.length, gameTime);
        builder.setMessage(messageString);
        builder.setPositiveButton("OK", (dialog, id) -> restart());
        builder.show();
    }

    private static class UIHandler extends Handler {
        //Weak reference protects against prevention of garbage collection.
        private final WeakReference<ReversiGame> reversiGameWeakReference;

        UIHandler(ReversiGame reversiGame) {
            reversiGameWeakReference = new WeakReference<>(reversiGame);
        }

        @Override
        public void handleMessage(Message msg) {
            ReversiGame reversiGame = reversiGameWeakReference.get();
            if (reversiGame != null) {
                reversiGame.handleUIMessage(msg);
            }
        }
    }

    private String getLastMovesString(){
        String lastMovesString = "";
        for(int i =1; i <= lastMovesArray.size(); ++i){
            lastMovesString = lastMovesString + " " + lastMovesArray.get(lastMovesArray.size()-i);
            if (i == 5){
                return lastMovesString;
            }
        }
        return lastMovesString;
    }

    public static class TileButton extends android.support.v7.widget.AppCompatImageButton {
        public TileButton(Context context) {
            super(context);
        }

        public TileButton(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public TileButton(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = MeasureSpec.getSize(heightMeasureSpec);
            int size = width > height ? height : width;
            setMeasuredDimension(size, size);
        }
    }
}
