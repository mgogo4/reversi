package world.mgogo.reversi;

import android.app.Activity;
import android.os.Bundle;

import world.mgogo.reversi.utils.ReversiConstants;


public class GameActivity extends Activity {

    private ReversiGame game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean whiteAI = getIntent().getBooleanExtra(ReversiConstants.WHITE_AI_KEY, true);
        boolean blackAI = getIntent().getBooleanExtra(ReversiConstants.BLACK_AI_KEY, true);

        game = new ReversiGame(this, whiteAI, blackAI);
        setContentView(game);
    }

    @Override
    protected void onResume() {
        super.onResume();
        game.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        game.pause();
    }
}
