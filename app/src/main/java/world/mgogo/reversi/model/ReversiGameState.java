package world.mgogo.reversi.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import sac.State;
import sac.StateFunction;
import sac.game.GameState;
import sac.game.GameStateImpl;
import world.mgogo.reversi.utils.AvailableMoves;
import world.mgogo.reversi.utils.ReversiConstants;

public class ReversiGameState extends GameStateImpl {
    private static final double AVAILABLE_MOVE_HEURISTIC_FUNCTION = 2.2D;

    static {
        setHFunction(new StateFunction() {
            @Override
            public double calculate(State state) {
                ReversiGameState reversiGameState = (ReversiGameState) state;
                double value = 0.0;

                if (reversiGameState.isFinished()) {
                    if (reversiGameState.getBlackScore() > reversiGameState.getWhiteScore()) {
                        return Double.NEGATIVE_INFINITY;
                    } else if (reversiGameState.getBlackScore() < reversiGameState.getWhiteScore()) {
                        return Double.POSITIVE_INFINITY;
                    }
                } else {
                    value = reversiGameState.board.getTilesValue() + AVAILABLE_MOVE_HEURISTIC_FUNCTION
                            * (reversiGameState.board.getBlackAvailableMoves().size()
                            - reversiGameState.board.getWhiteAvailableMoves().size());

/*                    if (reversiGameState.board.getBlackAvailableMoves().isEmpty()) {
                        value = value + 5.0D;
                    } else if (reversiGameState.board.getWhiteAvailableMoves().isEmpty()) {
                        value = value - 5.0D;
                    }*/
                }
                return value;
            }
        });
    }

    private byte actualPlayer; // BLACK == MAXIMIZING
    private Board board;

    public ReversiGameState() {
        actualPlayer = ReversiConstants.BLACK;
        board = new Board();
        this.setMaximizingTurnNow(true);
    }

    private ReversiGameState(ReversiGameState parent) {
        board = new Board(parent.board);
        actualPlayer = parent.actualPlayer;
        this.setMaximizingTurnNow(parent.isMaximizingTurnNow());
    }

    public boolean isFinished() {
        return board.getWhiteAvailableMoves().isEmpty() &&
                board.getBlackAvailableMoves().isEmpty();
    }

    public int getBlackScore() {
        return board.getBlackCount();
    }

    public int getWhiteScore() {
        return board.getWhiteCount();
    }

    public byte getState(int n) {
        return board.getTileState(n);
    }

    public int getPlayerScore(byte colour) {
        return board.getScore(colour);
    }

    public void restartGame() {
        actualPlayer = ReversiConstants.BLACK;
        this.setMaximizingTurnNow(true);
        board.resetBoard();
    }

    public byte getActualPlayer() {
        return actualPlayer;
    }

    public byte getWinner() {
        if (board.getScore(actualPlayer) > board.getScore((byte) (-1 * actualPlayer))) {
            return actualPlayer;
        } else if (board.getScore(actualPlayer) < board.getScore((byte) (-1 * actualPlayer))) {
            return (byte) (-1 * actualPlayer);
        }
        return ReversiConstants.EMPTY;
    }

    @Override
    public boolean isQuiet() {
        return isFinished() || !(board.getWhiteAvailableMoves().isEmpty() || board.getBlackAvailableMoves().isEmpty());
    }

    public AvailableMoves getPlayerAvailableMoves() {
        return board.getAvailableMoves(actualPlayer);
    }

    @Override
    public List<GameState> generateChildren() {
        AvailableMoves moves = board.getAvailableMoves(actualPlayer);
        List<GameState> children = new LinkedList<>();
        for (int i = 0; i < moves.size(); ++i) {
            ReversiGameState child = new ReversiGameState(this);
            child.makeMove(moves.getMove(i));
            child.setMoveName(String.valueOf(moves.getMove(i)));
            children.add(child);
        }
        return children;
    }

    public void makeMove(int n) {
        board.move(n, actualPlayer);
        if (!board.getAvailableMoves((byte) (-1 * actualPlayer)).isEmpty()) {
            actualPlayer = (byte) (-1 * actualPlayer);
            this.setMaximizingTurnNow(!isMaximizingTurnNow());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ReversiGameState that = (ReversiGameState) o;
        return actualPlayer == that.actualPlayer && Objects.equals(board, that.board);
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + actualPlayer;
        hash = 31 * hash + board.hashCode();
        return hash;
    }
}
