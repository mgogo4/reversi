package world.mgogo.reversi.model;

import world.mgogo.reversi.utils.ReversiConstants;

public class Player {
    private final boolean aiPlayer;
    private final String name;

    public Player(boolean aiPlayer, byte colour) {
        this.aiPlayer = aiPlayer;
        this.name = colour == ReversiConstants.WHITE ? "WHITE" : "BLACK";
    }

    public static String playerName(byte colour) {
        return colour == ReversiConstants.WHITE ? "WHITE" : "BLACK";
    }

    public boolean isAiPlayer() {
        return aiPlayer;
    }

    public String getName() {
        return name;
    }
}
