package world.mgogo.reversi.model;

import java.util.Arrays;

import world.mgogo.reversi.utils.AvailableMoves;
import world.mgogo.reversi.utils.ReversiConstants;
import world.mgogo.reversi.utils.ReversiUtils;

public class Board {
    private final AvailableMoves availableBlackMoves;
    private final AvailableMoves availableWhiteMoves;
    private final byte[] tiles = new byte[ReversiConstants.TILES_NUMBER];

    private int whiteCount;
    private int blackCount;

    private double tilesValue;


    Board() {
        availableBlackMoves = new AvailableMoves();
        availableWhiteMoves = new AvailableMoves();
        this.init();
    }

    private void init() {
        this.tiles[(ReversiConstants.SIZE / 2 - 1) * ReversiConstants.SIZE + ReversiConstants
                .SIZE / 2 - 1] = ReversiConstants.WHITE;
        this.tiles[(ReversiConstants.SIZE / 2 - 1) * ReversiConstants.SIZE + ReversiConstants
                .SIZE / 2] = ReversiConstants.BLACK;
        this.tiles[(ReversiConstants.SIZE / 2) * ReversiConstants.SIZE + ReversiConstants.SIZE /
                2 - 1] = ReversiConstants.BLACK;
        this.tiles[(ReversiConstants.SIZE / 2) * ReversiConstants.SIZE + ReversiConstants.SIZE /
                2] = ReversiConstants.WHITE;

        this.availableWhiteMoves.add((ReversiConstants.SIZE / 2 - 2) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2);
        this.availableWhiteMoves.add((ReversiConstants.SIZE / 2) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2 - 2);
        this.availableWhiteMoves.add((ReversiConstants.SIZE / 2 + 1) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2 - 1);
        this.availableWhiteMoves.add((ReversiConstants.SIZE / 2 - 1) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2 + 1);

        this.availableBlackMoves.add((ReversiConstants.SIZE / 2 - 2) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2 - 1);
        this.availableBlackMoves.add((ReversiConstants.SIZE / 2 - 1) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2 - 2);
        this.availableBlackMoves.add((ReversiConstants.SIZE / 2 + 1) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2);
        this.availableBlackMoves.add((ReversiConstants.SIZE / 2) * ReversiConstants.SIZE +
                ReversiConstants.SIZE / 2 + 1);

        this.tilesValue = 0.0D;

        this.blackCount = 2;
        this.whiteCount = 2;
    }

    Board(Board parent) {
        System.arraycopy(parent.tiles, 0, tiles, 0, ReversiConstants.TILES_NUMBER);


        whiteCount = parent.whiteCount;
        blackCount = parent.blackCount;

        tilesValue = parent.tilesValue;

        availableBlackMoves = new AvailableMoves(parent.availableBlackMoves);
        availableWhiteMoves = new AvailableMoves(parent.availableWhiteMoves);
    }

    public byte getTileState(int n) {
        return tiles[n];
    }

    public void resetBoard() {
        ReversiUtils.byteClear(tiles);
        clearAvailableMoves();
        init();
    }

    private void clearAvailableMoves() {
        availableBlackMoves.clear();
        availableWhiteMoves.clear();
    }

    public AvailableMoves getWhiteAvailableMoves() {
        return this.availableWhiteMoves;
    }

    public AvailableMoves getBlackAvailableMoves() {
        return this.availableBlackMoves;
    }

    public AvailableMoves getAvailableMoves(byte colour) {
        return colour == ReversiConstants.WHITE ? this.availableWhiteMoves : this
                .availableBlackMoves;
    }

    public int getScore(byte colour) {
        return colour == ReversiConstants.WHITE ? whiteCount : blackCount;
    }

    public double getTilesValue() {
        return tilesValue;
    }

    public int getWhiteCount() {
        return whiteCount;
    }

    public int getBlackCount() {
        return blackCount;
    }

    public void move(int n, byte colour) {
        this.tiles[n] = colour;
        int[] vectors = chooseVectors(n);
        for (int i = 0; i < vectors.length; ++i) {
            performMove(vectors[i], n, colour);
        }
        refreshAvailableMoves();
    }

    private static int[] chooseVectors(int n) {
        int modN = n % ReversiConstants.SIZE;
        if (modN < 2) {
            if (n <= 9) {
                return ReversiConstants.VECTORS_MIN_XY;
            } else if (n >= 48) {
                return ReversiConstants.VECTORS_MIN_X_MAX_Y;
            } else {
                return ReversiConstants.VECTORS_MIN_X;
            }
        } else if (modN >= 6) {
            if (n <= 15) {
                return ReversiConstants.VECTORS_MAX_X_MIN_Y;
            } else if (n >= 54) {
                return ReversiConstants.VECTORS_MAX_XY;
            } else {
                return ReversiConstants.VECTORS_MAX_X;
            }
        } else if (n < 16) {
            return ReversiConstants.VECTORS_MIN_Y;
        } else if (n > 47) {
            return ReversiConstants.VECTORS_MAX_Y;
        } else {
            return ReversiConstants.VECTORS;
        }
    }

    private void performMove(int vector, int start, byte colour) {
        int current = start + vector;
        byte currentTile = this.tiles[current];
        if (currentTile == (byte) (-1 * colour)) {
            current = current + vector;
            currentTile = this.tiles[current];

            if (currentTile == colour) {
                current = current - vector;
                while (current != start) {
                    this.tiles[current] = colour;
                    current = current - vector;
                }
                return;
            }
            while (tileExists(current + vector, current, currentTile)) {
                current = current + vector;
                currentTile = this.tiles[current];
                if (currentTile == colour) {
                    current = current - vector;
                    while (current != start) {
                        this.tiles[current] = colour;
                        current = current - vector;
                    }
                    return;
                }
            }
        }
    }

    private void refreshAvailableMoves() {
        clearAvailableMoves();
        calculateColourParameters();
    }

    private static boolean tileExists(int n, int previousN, byte colour) {
        int modN = n % ReversiConstants.SIZE;
        int modPreviousN = previousN % ReversiConstants.SIZE;
        return colour != ReversiConstants.EMPTY && n >= 0 && n < 64 && (modN != 0 || modPreviousN
                == 0 || modPreviousN == 1) && (modN != 7 || modPreviousN == 7 || modPreviousN == 6);
    }

    private void calculateColourParameters() {
        whiteCount = 0;
        blackCount = 0;

        tilesValue = 0.0D;

        for (int i = 0; i < ReversiConstants.TILES_NUMBER; ++i) {
            if (ReversiConstants.EMPTY == this.tiles[i]) {
                markAvailableMove(i);
            } else {
                if (ReversiConstants.WHITE == this.tiles[i]) {
                    ++whiteCount;
                    tilesValue = tilesValue - ReversiConstants.TILES_VALUES[i];
                } else {
                    ++blackCount;
                    tilesValue = tilesValue + ReversiConstants.TILES_VALUES[i];
                }
            }
        }
    }

    private void markAvailableMove(int n) {
        boolean whiteFound = false;
        boolean blackFound = false;
        byte foundMove;

        int[] vectors = chooseVectors(n);
        for (int i = 0; i < vectors.length; ++i) {
            foundMove = whoseAvailableMove(vectors[i], n);
            if (foundMove == ReversiConstants.BLACK && !blackFound) {
                this.availableBlackMoves.add(n);
                blackFound = true;
            } else if (foundMove == ReversiConstants.WHITE && !whiteFound) {
                this.availableWhiteMoves.add(n);
                whiteFound = true;
            }
            if (blackFound && whiteFound) {
                return;
            }
        }
    }

    private byte whoseAvailableMove(int vector, int start) {
        int current = start + vector;
        byte currentTile = this.tiles[current];
        if (currentTile != ReversiConstants.EMPTY) {
            current = current + vector;
            byte opposite = (byte) (-1 * currentTile);
            currentTile = this.tiles[current];

            if (currentTile == opposite) {
                return currentTile;
            }
            while (tileExists(current + vector, current, currentTile)) {
                current = current + vector;
                currentTile = this.tiles[current];
                if (currentTile == opposite) {
                    return currentTile;
                }
            }
        }
        return ReversiConstants.EMPTY;
    }

    @Override
    public int hashCode() {
        int hash = 17;

        hash = 31 * hash + Arrays.hashCode(tiles);

        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Board board = (Board) o;

        return whiteCount == board.whiteCount && blackCount == board.blackCount &&
                availableWhiteMoves.size() == board.availableWhiteMoves.size() &&
                availableBlackMoves.size() == board.availableBlackMoves.size() &&
                availableWhiteMoves.equals(board.availableWhiteMoves) &&
                availableBlackMoves.equals(board.availableBlackMoves) &&
                Arrays.equals(tiles, board.tiles);
    }
}
