package world.mgogo.reversi.utils;

import java.util.Arrays;

public class AvailableMoves {

    private static final int INITIAL_CAPACITY = 10;

    private int[] moves;

    private int capacity;
    private int size;

    public AvailableMoves(AvailableMoves parent) {
        moves = parent.moves.clone();

        size = parent.size;
        capacity = parent.capacity;
    }

    public AvailableMoves() {
        moves = new int[INITIAL_CAPACITY];
        size = 0;
        capacity = INITIAL_CAPACITY;
    }

    public int size() {
        return size;
    }

    public int getMove(int n) {
        return this.moves[n];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void add(int move) {
        ++size;
        if (size > capacity) {
            capacity = capacity + (capacity >> 1);
            if (size > capacity) {
                capacity = size;
            }

            int[] copy = new int[capacity];
            System.arraycopy(moves, 0, copy, 0, size - 1);
            moves = copy;
        }

        moves[size - 1] = move;
    }

    public boolean contains(int move) {
        for (int i = 0; i < size; ++i) {
            if (moves[i] == move) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        size = 0;
    }

    @Override
    public int hashCode() {
        int hash = 17;

        hash = 31 * hash + Arrays.hashCode(moves);

        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AvailableMoves availableMoves = (AvailableMoves) o;

        return Arrays.equals(moves, availableMoves.moves);
    }
}
