package world.mgogo.reversi.utils;

public final class ReversiConstants {
    public static final int SIZE = 8;
    public static final int LAST_TILE_IN_LINE = SIZE -1;
    public static final int LEFT_DOWN_CORNER_TILE = (SIZE -1)*SIZE;
    public static final int RIGHT_DOWN_CORNER_TILE = SIZE*SIZE -1;
    public static final int TILES_NUMBER = SIZE * SIZE;
    public static final byte EMPTY = 0;
    public static final byte BLACK = 1;
    public static final byte WHITE = -1;
    private static final int VECTOR_TO_RIGHT = 1;
    private static final int VECTOR_TO_RIGHT_DOWN = 1 + SIZE;
    private static final int VECTOR_TO_DOWN = SIZE;
    private static final int VECTOR_TO_LEFT_DOWN = -1 + SIZE;
    private static final int VECTOR_TO_LEFT = -1;
    private static final int VECTOR_TO_LEFT_UP = -1 - SIZE;
    private static final int VECTOR_TO_UP = -SIZE;
    private static final int VECTOR_TO_RIGHT_UP = 1 - SIZE;

    public static final int[] VECTORS = {
            VECTOR_TO_RIGHT,
            VECTOR_TO_RIGHT_DOWN,
            VECTOR_TO_DOWN,
            VECTOR_TO_LEFT_DOWN,
            VECTOR_TO_LEFT,
            VECTOR_TO_LEFT_UP,
            VECTOR_TO_UP,
            VECTOR_TO_RIGHT_UP
    };

    public static final int[] VECTORS_MIN_X = {
            VECTOR_TO_RIGHT,
            VECTOR_TO_RIGHT_DOWN,
            VECTOR_TO_DOWN,
            VECTOR_TO_UP,
            VECTOR_TO_RIGHT_UP};
    public static final int[] VECTORS_MIN_XY = {
            VECTOR_TO_RIGHT,
            VECTOR_TO_RIGHT_DOWN,
            VECTOR_TO_DOWN};
    public static final int[] VECTORS_MIN_X_MAX_Y = {
            VECTOR_TO_RIGHT,
            VECTOR_TO_UP,
            VECTOR_TO_RIGHT_UP};
    public static final int[] VECTORS_MAX_X = {
            VECTOR_TO_DOWN,
            VECTOR_TO_LEFT_DOWN,
            VECTOR_TO_LEFT,
            VECTOR_TO_LEFT_UP,
            VECTOR_TO_UP};
    public static final int[] VECTORS_MAX_XY = {
            VECTOR_TO_LEFT,
            VECTOR_TO_LEFT_UP,
            VECTOR_TO_UP};
    public static final int[] VECTORS_MAX_X_MIN_Y = {
            VECTOR_TO_DOWN,
            VECTOR_TO_LEFT_DOWN,
            VECTOR_TO_LEFT};
    public static final int[] VECTORS_MIN_Y = {
            VECTOR_TO_RIGHT,
            VECTOR_TO_RIGHT_DOWN,
            VECTOR_TO_DOWN,
            VECTOR_TO_LEFT_DOWN,
            VECTOR_TO_LEFT};
    public static final int[] VECTORS_MAX_Y = {
            VECTOR_TO_RIGHT,
            VECTOR_TO_LEFT,
            VECTOR_TO_LEFT_UP,
            VECTOR_TO_UP,
            VECTOR_TO_RIGHT_UP};
    public static final String WHITE_AI_KEY = "whiteAI";
    public static final String BLACK_AI_KEY = "blackAI";
    private static final double CORNER_VAL = 850.0;
    private static final double EDGE_VAL = 20.0;//15 17 - 47 (BLACK)
    private static final double EDGE_PLUS_VAL = 30.0;
    private static final double EDGE_MINUS_VAL = -10.0;
    private static final double INSIDE_CORNER_VAL = -200.0; //220
    private static final double INSIDE_EDGE_VAL = -40.0;//40
    private static final double MID_CORNER_VAL = 5.0;
    public static final double[] TILES_VALUES = {
            CORNER_VAL, EDGE_MINUS_VAL, EDGE_PLUS_VAL, EDGE_VAL, EDGE_VAL, EDGE_PLUS_VAL, EDGE_MINUS_VAL, CORNER_VAL,
            EDGE_MINUS_VAL, INSIDE_CORNER_VAL, INSIDE_EDGE_VAL, INSIDE_EDGE_VAL, INSIDE_EDGE_VAL, INSIDE_EDGE_VAL,
            INSIDE_CORNER_VAL, EDGE_MINUS_VAL,
            EDGE_PLUS_VAL, INSIDE_EDGE_VAL, MID_CORNER_VAL, 0.0, 0.0, MID_CORNER_VAL, INSIDE_EDGE_VAL, EDGE_PLUS_VAL,
            EDGE_VAL, INSIDE_EDGE_VAL, 0.0, 0.0, 0.0, 0.0, INSIDE_EDGE_VAL, EDGE_VAL,
            EDGE_VAL, INSIDE_EDGE_VAL, 0.0, 0.0, 0.0, 0.0, INSIDE_EDGE_VAL, EDGE_VAL,
            EDGE_PLUS_VAL, INSIDE_EDGE_VAL, MID_CORNER_VAL, 0.0, 0.0, MID_CORNER_VAL, INSIDE_EDGE_VAL, EDGE_PLUS_VAL,
            EDGE_MINUS_VAL, INSIDE_CORNER_VAL, INSIDE_EDGE_VAL, INSIDE_EDGE_VAL, INSIDE_EDGE_VAL, INSIDE_EDGE_VAL,
            INSIDE_CORNER_VAL, EDGE_MINUS_VAL,
            CORNER_VAL, EDGE_MINUS_VAL, EDGE_PLUS_VAL, EDGE_VAL, EDGE_VAL, EDGE_PLUS_VAL, EDGE_MINUS_VAL, CORNER_VAL};

    private ReversiConstants() {
    }
}
