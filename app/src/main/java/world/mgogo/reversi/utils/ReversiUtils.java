package world.mgogo.reversi.utils;

public final class ReversiUtils {


    /*
     * initialize a smaller piece of the array and use the System.arraycopy
     * call to fill in the rest of the array in an expanding binary fashion
     */
    public static void byteClear(byte[] array) {
        int len = array.length;

        if (len > 0){
            array[0] = 0;
        }

        //Value of i will be [1, 2, 4, 8, 16, 32, ..., len]
        for (int i = 1; i < len; i += i) {
            System.arraycopy(array, 0, array, i, ((len - i) < i) ? (len - i) : i);
        }
    }

    private ReversiUtils() {
    }
}
